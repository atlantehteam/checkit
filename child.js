(function($){
	function openExternalLinks() {
		const ignoreQuery = 'ciic=true'
		if (location.search.match(ignoreQuery)) {return;}
		document.addEventListener('click', function(event) {
			const ignoreClassNames = ['re_track_btn'];
			const currentClassList = Array.from(event.target.classList)
			if (ignoreClassNames.some(ig => currentClassList.includes(ig))) {return;}
			const targetHref = $('.ci_re_track_btn').attr('href') || $('.ci_re_track_btn a').attr('href') || $('.re_track_btn').attr('href');
			if (!targetHref) {return;}

			const search = location.search ? `${location.search}&${ignoreQuery}` : `?${ignoreQuery}`
			let newPage = `${location.origin}${location.pathname}${search}`;

			const aTarget = event.target.localName === 'a' ? event.target : $(event.target).closest('a')[0];
			if (aTarget) {
				if (aTarget.href === targetHref) {
					return;
				}
				event.preventDefault();
				newPage = `${aTarget.href}?${ignoreQuery}`
			}
			
			window.open(newPage);
			location.href = targetHref;
		})

	}

	// openExternalLinks();
}(jQuery))