<?php

add_action( 'wp_enqueue_scripts', 'enqueue_parent_theme_style' );
function enqueue_parent_theme_style() {
    wp_enqueue_style( 'parent-style', get_template_directory_uri().'/style.css' );
	if (is_rtl()) {
		 wp_enqueue_style( 'parent-rtl', get_template_directory_uri().'/rtl.css', array(), RH_MAIN_THEME_VERSION);
	}

	if (!current_user_can('administrator')) {
		$cache_buster = date("YmdHi", filemtime( get_stylesheet_directory() . '/child.js'));
		wp_enqueue_script('child-js', get_stylesheet_directory_uri() . '/child.js', ['jquery'], $cache_buster, true);
	}
}

?>